$(document).ready(function () {
  function prefixIDs(svgElement, prefix) {
    const elementsWithID = svgElement.querySelectorAll("[id]");
    const idMap = new Map();

    // Update IDs and create a map of old ID to new ID
    elementsWithID.forEach((el) => {
      const oldID = el.id;
      const newID = prefix + oldID;
      idMap.set(oldID, newID);
      el.id = newID;
    });

    // Explicitly update xlink:href attributes
    svgElement.querySelectorAll("use").forEach((useEl) => {
      const href = useEl.getAttribute("xlink:href");
      if (href && href.startsWith("#")) {
        const oldID = href.substring(1);
        const newID = idMap.get(oldID);
        if (newID) {
          useEl.setAttribute("xlink:href", `#${newID}`);
        }
      }
    });
  }

  const svg1 = document.getElementById("svg-plot-03");

  prefixIDs(svg1, "svg03-");
});
