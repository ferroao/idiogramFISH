FROM rocker/r-base:latest

LABEL maintainer="Fernando Roa <froao@unal.edu.co>"

RUN apt-get update && apt-get install -y --no-install-recommends \
    sudo \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    libssl-dev \
    libssh2-1-dev \
    libxml2-dev \
    && rm -rf /var/lib/apt/lists/*

RUN wget https://github.com/jgm/pandoc/releases/download/2.13/pandoc-2.13-1-amd64.deb
RUN dpkg -i pandoc-2.13-1-amd64.deb
RUN pandoc --version
RUN R -q -e "install.packages('rmarkdown',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('rhandsontable',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('rclipboard',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('clipr',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('knitr',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('gtools',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('rentrez',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -q -e "install.packages('https://cran.r-project.org/src/contrib/Archive/shiny/shiny_1.6.0.tar.gz', repos = NULL, type ='source')"
RUN R -q -e "install.packages('shinydashboard',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN echo "local(options(shiny.port = 8080, shiny.host = '0.0.0.0'))" > /usr/lib/R/etc/Rprofile.site

COPY packagetar .
RUN R -q -e "install.packages('idiogramFISH_2.0.6.tar.gz', repos = NULL, type ='source')"
#RUN R -q -e "install.packages('idiogramFISH',dependencies=TRUE, repos='http://cran.rstudio.com/')"

EXPOSE 8080
CMD ["R", "-e", "shiny::runApp(system.file('shinyApps', 'iBoard', package = 'idiogramFISH'),launch.browser=F)"]
