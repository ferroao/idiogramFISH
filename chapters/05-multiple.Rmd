# Multiple OTUs

```{r , echo = FALSE, results = "asis", message = FALSE, eval = TRUE, comment = NA}
note <- "05-multiple.ipynb"
pasteLinks(note)
``` 


## Example with several species (OTUs) - mono.

To illustrate this, we will load some data.frames from the package 

* Chromosome sizes 

<div class="pre-scrollx">
```
require(idiogramFISH)
head(bigdfOfChrSize, 15)
```
</div>

<div class="verysmall">
```{r, echo = F, message = F}
require(idiogramFISH)
kableExtra::kable_styling(knitr::kable(head(bigdfOfChrSize, 15)), full_width = FALSE,
  font_size = 10,
  bootstrap_options = c("striped", "hover", "condensed"),

)
```
</div>

* Mark characteristics, does not require OTU 

<div class="nobullet">
* df optional for ver > 1.0.0   
</div>

<div class="pre-scrollx">
```
data("dfMarkColor")
```
</div>

<div class="verysmall">
```{r, echo = F}
kableExtra::kable_styling(knitr::kable(dfMarkColor), full_width = FALSE,
  font_size = 10,
  bootstrap_options = c("striped", "hover", "condensed")
)
```
</div>

* Mark position

<div class="pre-scrollx">
```
data("bigdfOfMarks")
```
</div>

<div class="verysmall">
```{r, echo = F}
kableExtra::kable_styling(knitr::kable(bigdfOfMarks), full_width = FALSE,
  font_size = 10,
  bootstrap_options = c("striped", "hover", "condensed")
)
```
</div>

### Plotting {-}

<div class="fold s">
```{r example_M3, echo = TRUE, results = "hide", fig.width = 6, fig.height = 13, message = FALSE, dev = 'png'}
# png("bigdfOfChrSize.png", width = 650, height = 1300)
par(mar = c(0, 0, 0, 0))
plotIdiograms(dfChrSize  = bigdfOfChrSize, # chr. sizes
  dfMarkColor = dfMarkColor,  # mark characteristics, optional in dev version. see above.
  dfMarkPos  = bigdfOfMarks,  # mark positions (inc. cen. marks)

  karHeight = 2.5,           # karyotype rel. height
  karHeiSpace = 6,           # karyotype vertical size with spacing
  chrWidth = .35,            # chr. width
  amoSepar = 2,              # Vertical separation of kar. when karSepar = TRUE

  squareness = 4,            # squareness of chr. vertices
  distTextChr = .8,          # distance of chr. to text
  chrIndex = "AR",           # add arm ratio only. For v. >=1.12
  nameChrIndexPos = 3,
  morpho = "Guerra",           # add chr. morphology by Guerra, see above. For v. >=1.12
  indexIdTextSize = .6,        # font size of indices and chr. name
  OTUTextSize = .9,            # font size of OTU names

  markLabelSize = .7,          # font size of legend
  fixCenBorder = TRUE,         # use chrColor as border color of cen. or cen. marks
  legendHeight = 2,            # height of labels

  rulerPos = -1,               # position of ruler
  ruler.tck = -0.004,          # size and orient. of ticks in ruler
  rulerNumberPos = .4,         # position of numbers of ruler
  rulerNumberSize = .4,        # font size of ruler
  xPosRulerTitle = 5,          # ruler units pos.
  rulerTitleSize = .5,         # ruler font size of units (title)

  xlimRightMod = 3,          # modify xlim left argument
  xlimLeftMod = 2,           # modify xlim left argument
  ylimBotMod = 0,            # modify ylim bottom argument
  ylimTopMod = -.3           # modify ylim top argument
)
# dev.off() # for png()
```
</div>

```{r, results = "asis", comment = NA, echo = FALSE, eval = FALSE}
cat(paste0("![](bigdfOfChrSize.png)"))
```

<div class="goright95">
<a href="#content">top <i class="fa fa-caret-square-o-up" aria-hidden="true"></i></a>
</div>

## Example with several species (OTUs) - holo.

To illustrate this, we will load some data.frames from the package 

* Chromosome sizes

<div class="pre-scrollx">
```
data(bigdfChrSizeHolo)
```
</div>

<div class="verysmall">
```{r, echo = F}
kableExtra::kable_styling(knitr::kable(bigdfChrSizeHolo), full_width = FALSE,
  font_size = 10, position = "center")
```
</div>

* Mark characteristics, does not require OTU

<div class="nobullet">
* d.f optional for ver. > 1.0.0
</div>

<div class="pre-scrollx">
```
data(dfMarkColorIn) 
```
</div>

<div class="verysmall">
```{r, echo = F}
kableExtra::kable_styling(knitr::kable(dfMarkColorIn), full_width = FALSE,
  font_size = 10)
```
</div>

* Mark position

<div class="pre-scrollx">
```
data(bigdfMarkPosHolo2)
```
</div>

<div class="verysmall">
```{r, echo = F}
# just to show it here
kableExtra::kable_styling(knitr::kable(bigdfMarkPosHolo2), full_width = FALSE,
  font_size = 10)
```
</div>

### Plotting {-}

<div class="fold s">
```{r , echo = TRUE, results = "hide", fig.width = 6, fig.height = 6, message = FALSE, dev = 'png', eval = FALSE}
library(idiogramFISH)

# fig.width = 6, fig.height = 6
png("bigdfChrSizeHolo.png", width = 600, height = 600)
par(mar = rep(0, 4))

plotIdiograms(dfChrSize = bigdfChrSizeHolo, # chr. size data.frame
  dfMarkColor = dfMarkColorIn,   # df of mark style
  dfMarkPos = bigdfMarkPosHolo2, # df of marks' position

  markDistType = "cen",         # measure towards center of mark
  squareness = 6,               # vertices squareness of chr. and marks

  karHeiSpace = 4,            # karyotype height including spacing
  karSepar = TRUE,            # reduce vertical space among karyotypes
  amoSepar = 1,               # separation among karyotypes
  distTextChr = .5,           # distance text to chr.

  legendWidth = 1             # width of legend labels

  , chrId = "simple",           # numbering of chr., not using "original" name

  indexIdTextSize = .9,         # font size of chr names and indices
  markLabelSize = .9,           # font size of legends

  rulerPos = 0,                 # position of ruler
  rulerNumberSize = .9,         # font size of ruler
  ruler.tck = -.004,            # tick length and orient.
  xPosRulerTitle = 3,           # position of ruler units (title)

  ylimBotMod = .4               # modify ylim bottom argument
  , xModifier = 50              # separation among chromatids
)
dev.off()
```
</div>
```{r, echo = FALSE, results = "hide", fig.width = 6, fig.height = 6, message = FALSE, dev = 'png', eval = TRUE}
library(idiogramFISH)

# fig.width = 6, fig.height = 6
png("../vignettes/bigdfChrSizeHolo.png", width = 600, height = 600)
par(mar = rep(0, 4))

plotIdiograms(dfChrSize = bigdfChrSizeHolo, # chr. size data.frame
  dfMarkColor = dfMarkColorIn,   # df of mark style
  dfMarkPos = bigdfMarkPosHolo2, # df of marks' position

  markDistType = "cen",         # measure towards center of mark
  squareness = 6,               # vertices squareness of chr. and marks

  karHeiSpace = 4,              # karyotype height including spacing
  karSepar = TRUE,              # reduce vertical space among karyotypes
  amoSepar = 1,                 # separation among karyotypes
  distTextChr = .5,             # distance text to chr.

  legendWidth = 1               # width of legend labels

  , chrId = "simple",           # numbering of chr., not using "original" name

  indexIdTextSize = .9,         # font size of chr names and indices
  markLabelSize = .9,           # font size of legends

  rulerPos = 0,                 # position of ruler
  rulerNumberSize = .9,         # font size of ruler
  ruler.tck = -.004,            # tick length and orient.
  xPosRulerTitle = 3,           # position of ruler units (title)

  ylimBotMod = .4               # modify ylim bottom argument
  , xModifier = 50              # separation among chromatids
)
dev.off()
```


```{r, results = "asis", comment = NA, echo = FALSE}
bigdfChrSizeHolo.png <- "../vignettes/bigdfChrSizeHolo.png"
bigdfChrSizeHolo.png <- normalizePath(bigdfChrSizeHolo.png)
cat(paste0("![](", bigdfChrSizeHolo.png, ")"))
```


<div class="goright95">
<a href="#content">top <i class="fa fa-caret-square-o-up" aria-hidden="true"></i></a>
</div>
